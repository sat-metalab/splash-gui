from enum import IntEnum, unique
from typing import Generic, TypeVar, TYPE_CHECKING

from satnet.request import ClientRequest, ClientResponse, ServerRequest, ServerResponse

if TYPE_CHECKING:
    # noinspection PyUnresolvedReferences
    from guash.server.session import GuashRemoteSession
    # noinspection PyUnresolvedReferences
    from guash.client.session import GuashLocalSession


@unique
class RequestId(IntEnum):
    """
    Guash Request Ids
    """
    OPEN_SINK_BUFFER = 0x01
    CLOSE_SINK_BUFFER = 0x02
    MEDIA_DISCOVERY = 0x03
    MEDIA_INFO = 0x04
    GET_ATTRIBUTE = 0x05
    OBJECTS_OF_TYPE = 0x06
    ACTIVATE_MEDIA = 0x07


@unique
class ResponseId(IntEnum):
    """
    Guash Response Ids
    """
    OPEN_SINK_BUFFER = 0x01
    CLOSE_SINK_BUFFER = 0x02
    MEDIA_DISCOVERY = 0x03
    MEDIA_INFO = 0x04
    GET_ATTRIBUTE = 0x05
    OBJECTS_OF_TYPE = 0x06
    ACTIVATE_MEDIA = 0x07


CR = TypeVar('CR', bound='ClientRequest')
SR = TypeVar('SR', bound='ServerRequest')


class GuashClientRequest(ClientRequest['GuashRemoteSession']):
    """ Helper wrapper to save on imports and generic type declarations in concrete guash server requests """
    pass


class GuashClientResponse(Generic[SR], ClientResponse['GuashRemoteSession', SR]):
    """ Helper wrapper to save on imports and generic type declarations in concrete guash server responses """
    pass


class GuashServerRequest(ServerRequest['GuashLocalSession']):
    """ Helper wrapper to save on imports and generic type declarations in concrete guash server requests """
    pass


class GuashServerResponse(Generic[CR], ServerResponse['GuashLocalSession', CR]):
    """ Helper wrapper to save on imports and generic type declarations in concrete guash server responses """
    pass
