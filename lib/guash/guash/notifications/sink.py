from typing import Optional, TYPE_CHECKING

from guash.notification import GuashServerNotification, NotificationId
from satnet.notification import notification

if TYPE_CHECKING:
    from guash.client.session import GuashLocalSession


@notification(id=NotificationId.SINK_RENDER)
class SinkRender(GuashServerNotification):
    """

    """
    _fields = ['buffer', 'width', 'height']

    def __init__(self, buffer: Optional[bytes] = None, width: Optional[int] = None, height: Optional[int] = None) -> None:
        super().__init__()
        self.buffer = buffer or b''
        self.width = width or 0
        self.height = height or 0

    def handle(self, session: 'GuashLocalSession') -> None:
        session.client.app.guash_root.set_render(self.buffer, self.width, self.height)
