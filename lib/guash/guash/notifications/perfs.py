from typing import Optional, TYPE_CHECKING

from guash.notification import GuashServerNotification, NotificationId
from satnet.notification import notification

if TYPE_CHECKING:
    from guash.client.session import GuashLocalSession


@notification(id=NotificationId.MASTER_CLOCK)
class MasterClock(GuashServerNotification):
    """
    Notification used to update the master clock on clients
    """
    _fields = ['clock']

    def __init__(self, clock: Optional[int] = None) -> None:
        super().__init__()
        self.clock = clock

    def handle(self, session: 'GuashLocalSession') -> None:
        session.client.app.guash_root.set_clock(self.clock, True)


@notification(id=NotificationId.FPS)
class GetFPS(GuashServerNotification):
    """
    Notification used to update the current framerate on all clients
    """
    _fields = ['fps']

    def __init__(self, fps: Optional[float] = None) -> None:
        super().__init__()
        self.fps = fps

    def handle(self, session: 'GuashLocalSession') -> None:
        session.client.app.guash_root.set_fps(self.fps)
