import logging
from typing import Optional, TYPE_CHECKING

if TYPE_CHECKING:
    from guash.client.app import GuashApp

from guash.client.session import GuashLocalSession
from satlib.tasks import TaskManager
from satnet.adapters.websockets.client import WSClient
from satnet.client import Client

logger = logging.getLogger(__name__)


class GuashClient(Client):
    def __init__(self, adapter: WSClient):
        super().__init__(session=GuashLocalSession(client=self), adapter=adapter)
        self._app = None  # type: 'GuashApp'
        self._task_manager = TaskManager()

    @property
    def app(self) -> 'GuashApp':
        return self._app

    @app.setter
    def app(self, value) -> None:
        self._app = value

    @property
    def task_manager(self):
        return self._task_manager

    def step(self, now: int, dt: int) -> None:
        super().step(now, dt)
        self._task_manager.step(now=now)

    def connect(self, host: Optional[str] = 'localhost', port: Optional[int] = 8765) -> None:
        super().connect(host=host, port=port)
