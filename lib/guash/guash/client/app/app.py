import time
from os.path import dirname, join

import kivy
from kivy.app import App
from kivy.clock import Clock
from kivy.config import Config

from guash.client.app.guash import Guash
# Needed to build the kv language file but will be removed by IDE if not ignored
# noinspection PyUnresolvedReferences
from guash.client.app.selectable_list import SelectableRecycleBoxLayout
from guash.client.app.settings import GuashSettings
from guash.client.client import GuashClient

kivy.require('1.10.0')
__version__ = "0.0.1"


class GuashApp(App):
    def __init__(self, client: GuashClient):
        super().__init__()
        self._client = client
        self._guash_root = None  # type: Guash
        self.use_kivy_settings = False
        self.settings_cls = GuashSettings
        Config.read(join(dirname(__file__), 'config', 'config.ini'))

        # Check messages once every 1 ms
        self._message_loop = Clock.schedule_interval(lambda dt: self._client.step(time.time(), dt), 0.001)

    @property
    def guash_root(self) -> Guash:
        """
        Access the root widget of the application
        :return: The root widget of the Kivy application.
        """
        return self._guash_root

    def build(self) -> Guash:
        self._guash_root = Guash(self._client)
        return self._guash_root

    def open_settings(self):
        self._guash_root.fetch_objects_of_type('image')
        super().open_settings()
