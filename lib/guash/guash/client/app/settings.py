from os.path import join, dirname

import kivy.utils as utils
from kivy.app import App
from kivy.config import Config
from kivy.core.window import Window
from kivy.metrics import dp
from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.colorpicker import ColorPicker
from kivy.uix.popup import Popup
from kivy.uix.settings import Settings, SettingItem, SettingOptions, SettingSpacer


class SettingDynamicOptions(SettingOptions):
    # The app_function must be a property because we do not call it with (). it will crash otherwise
    app_function = StringProperty()

    def _create_popup(self, instance):
        self.options = getattr(App.get_running_app().guash_root, str(self.app_function))
        super()._create_popup(instance)


class SettingColorPicker(SettingItem):
    """
    Color picker option for settings, it is stored as an hex string and displayed in a label but provides a color
    picker UI
    """

    popup = ObjectProperty(None, allownone=True)
    color_string = ObjectProperty(None)
    color_picker = None  # type: ColorPicker

    def on_panel(self, instance, value):
        if value is None:
            return
        self.bind(on_release=self._create_popup)

    def _dismiss(self, *largs):
        if self.color_string:
            self.color_string.focus = False
        if self.popup:
            self.popup.dismiss()
        self.popup = None

    def _validate(self, instance):
        self._dismiss()
        value = utils.get_hex_from_color(self.color_picker.color)
        self.value = value
        App.get_running_app().guash_root.apply_colors()

    def _create_popup(self, instance):
        content = BoxLayout(orientation='vertical', spacing='5dp')
        popup_width = min(0.95 * Window.width, dp(500))
        self.popup = Popup(title=self.title, content=content, size_hint=(None, 0.9), width=popup_width)

        if not self.value:
            self.value = Config.get('ui', 'bg_color') or '#000000FF'

        self.color_picker = ColorPicker(color=utils.get_color_from_hex(self.value))
        self.color_picker.bind(on_color=self._validate)

        buttons = BoxLayout(size_hint_y=None, height='50dp', spacing='5dp')
        btn = Button(text='Ok')
        btn.bind(on_release=self._validate)
        buttons.add_widget(btn)
        btn = Button(text='Cancel')
        btn.bind(on_release=self._dismiss)
        buttons.add_widget(btn)

        content.add_widget(self.color_picker)
        content.add_widget(SettingSpacer())
        content.add_widget(buttons)

        self.popup.open()


class GuashSettings(Settings):
    def __init__(self):
        super().__init__()
        self.register_type('dynamic_option', SettingDynamicOptions)
        self.register_type('color_picker', SettingColorPicker)

        self.add_json_panel(title='Guash settings', config=Config,
                            filename=join(dirname(__file__), 'config', 'settings.json'))
        self.interface.bind(on_close=lambda k: App.get_running_app().guash_root.apply_settings())
