import logging
from operator import itemgetter
from typing import List, Dict

import kivy.utils as utils
from kivy.config import Config
from kivy.graphics.texture import Texture
from kivy.properties import ListProperty, NumericProperty, ObjectProperty, StringProperty, ColorProperty
from kivy.uix.checkbox import CheckBox
from kivy.uix.floatlayout import FloatLayout

from guash.client.app.timeline import Timeline
from guash.client.client import GuashClient
from guash.commands.attributes import SetWorldAttribute
from guash.commands.projects import LoadProject
from guash.commands.media import PreviewMedia
from guash.commands.subscriptions import SubscribeNotif, UnsubscribeNotif
from guash.notification import NotificationId
from guash.requests.media_discovery import MediaDiscoveryRequest, ActivateMediaRequest
from guash.requests.media_info import MediaInfoRequest
from guash.requests.object_discovery import ObjectsOfTypeRequest
from guash.requests.sink import OpenSinkRequest, CloseSinkRequest
from satlib.tasks import Task

logger = logging.getLogger(__name__)


class Guash(FloatLayout):
    # Properties needed when building the UI
    _clock = StringProperty()
    _fps = StringProperty()
    _media = ListProperty()
    _active_media = ListProperty()
    _activating_media = ListProperty()
    _projects = ListProperty()
    _sink_texture = ObjectProperty()
    _background_color = ColorProperty()
    _selected_color = ColorProperty()

    _media_width = NumericProperty(0)
    _media_height = NumericProperty(0)
    _media_bpp = NumericProperty(0)
    _media_channels = NumericProperty(0)
    _media_format = StringProperty("")
    _media_duration = NumericProperty(0)

    def __init__(self, client: GuashClient):
        super().__init__()
        self._client = client
        self._clock_ms = 0  # type: int
        self._clock_task = None  # type: Task
        self._image_objects = []  # type: List[str]

        dim = int(Config.get('ui', 'texture_dimension'))
        self._sink_texture = Texture.create(size=(dim, dim), colorfmt='rgba')
        self._sink_texture.flip_vertical()

        self._background_color = Config.get('ui', 'bg_color')
        self._selected_color = (.0, 0.9, .1, .3)

        self._current_render = False

        self._timeline = Timeline(
            self._client.task_manager.create_task(callback=lambda: self._timeline.update(), frequency=60),
            self.ids.timeline_layout
        )

    @property
    def image_objects(self) -> List[str]:
        return self._image_objects

    @image_objects.setter
    def image_objects(self, value: List[str]) -> None:
        self._image_objects = value

    # Commands features
    def connect(self) -> None:
        self._client.connect(host=Config.get('connection', 'ipaddr'), port=Config.get('connection', 'port'))

    def toggle_wireframe(self, checkbox: CheckBox, value: bool) -> None:
        self._client.session.command(SetWorldAttribute('wireframe', [value]))

    def save_project(self, obj) -> None:
        pass

    def load_project(self) -> None:
        project_list = self.ids.project_files
        selected_index = project_list.ids.selected.index
        if not selected_index:
            return
        selected_project = project_list.data[selected_index]
        if not selected_project:
            return
        self._client.session.command(LoadProject(selected_project.get('text')))

    # Style settings methods
    def apply_colors(self):
        self._background_color = utils.get_color_from_hex(Config.get('ui', 'bg_color'))

    def apply_settings(self):
        self.apply_colors()

    # Media features
    def request_media_files(self) -> None:
        """
        Updates the media lists (active and inactive)
        """
        self._client.session.request(MediaDiscoveryRequest('mediaDirectory'))
        self._client.session.request(MediaDiscoveryRequest('activeMediaDirectory'))

    def request_project_files(self) -> None:
        self._client.session.request(MediaDiscoveryRequest('projectDirectory', ['.json']))

    def update_file_list(self, file_list: List[Dict], directory: str) -> None:
        if directory == 'projectDirectory':
            self._projects = file_list
        elif directory == 'mediaDirectory':
            self._media = file_list
        elif directory == 'activeMediaDirectory':
            self._active_media = file_list

    def update_media_info(self, media_info: Dict, filename: str) -> None:
        for m in self._media:
            if m.get('text') == filename:
                self._media_width = m['media_width'] = media_info.get('media_width') or 0
                self._media_height = m['media_height'] = media_info.get('media_height') or 0
                self._media_bpp = m['media_bpp'] = media_info.get('media_bpp') or 0
                self._media_channels = m['media_channels'] = media_info.get('media_channels') or 0
                self._media_format = m['media_format'] = media_info.get('media_format') or ''
                if m.get('live'):
                    self._media_duration = m['media_duration'] = int(Config.get('ui', 'default_duration'))
                else:
                    self._media_duration = m['media_duration'] = media_info.get('media_duration') or 0
                break

    def activate_media(self, index: int) -> None:
        if index is None:
            return
        media = self._media[index]
        filename = media.get('text') or ''
        if filename in [m.get('text') or '' for m in self._activating_media]:
            return
        if not self._activating_media:
            self._activating_media = [media]
        else:
            self._activating_media.append(media)
        self._activating_media = sorted(self._activating_media, key=itemgetter('text'))
        self._client.session.request(ActivateMediaRequest(filename))

    def finished_activation(self, filename: str) -> None:
        dic = {'text': filename}
        self._activating_media.remove(dic)
        if filename in [m['text'] for m in self._active_media]:
            return
        self._active_media.append(dic)
        self._active_media = sorted(self._active_media, key=itemgetter('text'))

    # Performance and timing
    def set_clock(self, clock: int, reset_task: bool = False) -> None:
        if clock:
            self._clock_ms = clock
            hours = clock // 3600000
            rem = clock % 3600000
            minutes = rem // 60000
            rem = rem % 60000
            seconds = rem // 1000
            rem = rem % 1000
            self._clock = '{}:{}:{}.{}'.format(str(hours), str(minutes), str(seconds), str(rem))
            if reset_task:
                if self._clock_task:
                    self._client.task_manager.remove_task(self._clock_task)
                self._clock_task = self._client.task_manager.create_task(
                    callback=lambda: self.set_clock(self._clock_ms + 1000 // 60),
                    frequency=60
                )
        elif self._clock_task:
            self._client.task_manager.remove_task(self._clock_task)

    def toggle_looseclock(self, checkbox: CheckBox, value: bool) -> None:
        self._client.session.command(SetWorldAttribute('looseClock', [value]))

    def set_fps(self, fps: float) -> None:
        self._fps = '{:.2f}'.format(fps)

    # Render previews
    @staticmethod
    def get_render_target_image() -> str:
        target_image = Config.get('splash', 'target_image')
        if not target_image:
            logger.warning('You must select a target image in the settings to preview its render.')
            return ''
        return target_image

    def request_current_render(self):
        target_image = self.get_render_target_image()
        if not target_image:
            return

        if not self._current_render:
            self.request_render(target_image)
            self._current_render = True
        else:
            self.request_stop_render(target_image)
            self._current_render = False

    def request_preview_media(self, index: int) -> None:
        if index == -1:
            return
        filename = self._media[index]['text']
        if filename not in [m['text'] for m in self._active_media]:
            return
        self._client.session.command(PreviewMedia(filename))
        self.request_render('preview_media')

    def request_media_info(self, index: int) -> None:
        if index is None or index == -1:
            return
        filename = self._media[index]['text']
        self._client.session.request(MediaInfoRequest(filename))

    def request_render(self, target_image: str) -> None:
        self.stop_all_renders()
        self._client.session.request(OpenSinkRequest(target_image))
        self._client.session.command(SubscribeNotif(NotificationId.SINK_RENDER))

    def stop_all_renders(self):
        self.request_stop_render(self.get_render_target_image())
        self.request_stop_render('preview_media')

    def request_stop_render(self, target_image: str) -> None:
        self._client.session.command(UnsubscribeNotif(NotificationId.SINK_RENDER))
        self._client.session.request(CloseSinkRequest(target_image))

    def set_render(self, buffer: bytes, width: int, height: int) -> None:
        if not len(buffer):
            dim = int(Config.get('ui', 'texture_dimension'))
            buffer = bytes([0 for _ in range(dim * dim * 4)])
            width = dim
            height = dim

        if width != self._sink_texture.width or height != self._sink_texture.height:
            self._sink_texture = Texture.create(size=(width, height), colorfmt='rgba')
            self._sink_texture.flip_vertical()
        self._sink_texture.blit_buffer(buffer, colorfmt='rgba')
        self.ids.render_image.canvas.ask_update()

    def flush_render(self, buffer: bytes) -> None:
        dim = int(Config.get('ui', 'texture_dimension'))
        self._sink_texture.blit_buffer(bytes([0 for _ in range(dim * dim * 4)]), colorfmt='rgba')
        self.ids.render_image.canvas.ask_update()

    def fetch_objects_of_type(self, obj_type: str) -> None:
        self._client.session.request(ObjectsOfTypeRequest(obj_type))

    def add_to_timeline(self, index: int) -> None:
        if index == -1:
            return
        media = self._media[index]
        if media['text'] not in [m['text'] for m in self._active_media]:
            return
        self._timeline.add_media(media)
