import logging
from typing import List, TYPE_CHECKING

from satnet.client import LocalSession

if TYPE_CHECKING:
    from guash.client.client import GuashClient

logger = logging.getLogger(__name__)


class GuashLocalSession(LocalSession):
    """
    Guash local session
    """

    def __init__(self, client: 'GuashClient') -> None:
        # Init `GuashSession` first as `LocalSession` will apply a callback
        super().__init__(client=client)
        self._project_files = []  # type: List[str]
        self._media_files = []  # type: List[str]

    @property
    def project_files(self):
        return self._project_files

    @project_files.setter
    def project_files(self, value):
        self._project_files = value

    @property
    def media_files(self):
        return self._media_files

    @media_files.setter
    def media_files(self, value):
        self._media_files = value
