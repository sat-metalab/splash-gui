import logging
from typing import TYPE_CHECKING

from guash.notification import NotificationId
from satnet.server import RemoteSession

if TYPE_CHECKING:
    from guash.server.server import GuashServer

logger = logging.getLogger(__name__)


class GuashRemoteSession(RemoteSession):
    """
    Guash remote session
    """

    def __init__(self, id: int, server: 'GuashServer') -> None:
        super().__init__(id=id, server=server)
        self._sink = None
        self.subscribe(NotificationId.MASTER_CLOCK)
        self.subscribe(NotificationId.FPS)
