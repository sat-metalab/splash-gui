import logging
from typing import Optional, TYPE_CHECKING

from guash.command import CommandId, GuashClientCommand
from guash.notification import NotificationId
from satnet.command import command

if TYPE_CHECKING:
    from guash.server.session import GuashRemoteSession

logger = logging.getLogger(__name__)


@command(id=CommandId.SUBSCRIBE)
class SubscribeNotif(GuashClientCommand):
    _fields = ['notif']

    def __init__(self, notif: Optional[NotificationId] = None) -> None:
        super().__init__()
        self.notif = notif

    def handle(self, session: 'GuashRemoteSession') -> None:
        if self.notif:
            session.subscribe(self.notif)


@command(id=CommandId.UNSUBSCRIBE)
class UnsubscribeNotif(GuashClientCommand):
    _fields = ['notif']

    def __init__(self, notif: Optional[NotificationId] = None) -> None:
        super().__init__()
        self.notif = notif

    def handle(self, session: 'GuashRemoteSession') -> None:
        if self.notif:
            session.unsubscribe(self.notif)
