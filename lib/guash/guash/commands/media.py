import logging
from typing import Optional, TYPE_CHECKING

from guash.command import CommandId, GuashClientCommand
from satnet.command import command

if TYPE_CHECKING:
    from guash.server.session import GuashRemoteSession

logger = logging.getLogger(__name__)


@command(id=CommandId.PREVIEW_MEDIA)
class PreviewMedia(GuashClientCommand):
    _fields = ['filename']

    def __init__(self, filename: Optional[str] = None) -> None:
        super().__init__()
        self.filename = filename

    def handle(self, session: 'GuashRemoteSession') -> None:
        session.server.controller.preview_media(self.filename)
