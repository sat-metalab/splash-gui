from enum import IntEnum, unique
from typing import TYPE_CHECKING

from satnet.notification import ServerNotification, ClientNotification

if TYPE_CHECKING:
    # noinspection PyUnresolvedReferences
    from guash.client.session import GuashLocalSession
    # noinspection PyUnresolvedReferences
    from guash.server.session import GuashRemoteSession


@unique
class NotificationId(IntEnum):
    """
    Guash Notification Ids
    """
    MASTER_CLOCK = 0x01
    FPS = 0x02
    SINK_RENDER = 0x03


class GuashClientNotification(ClientNotification['GuashRemoteSession']):
    """
    Client notification, can only be used by clients
    Helper wrapper to save on imports and generic type declarations in concrete guash commands
    """
    pass


class GuashServerNotification(ServerNotification['GuashLocalSession']):
    """
    Server notification, can only be used by servers
    Helper wrapper to save on imports and generic type declarations in concrete guash commands
    """
    pass
