import logging
from typing import List, Optional, TYPE_CHECKING

from guash.request import RequestId, ResponseId, GuashClientRequest, GuashServerResponse
from satnet.request import request, response, ResponseCallback

if TYPE_CHECKING:
    from guash.client.session import GuashLocalSession
    from guash.server.session import GuashRemoteSession

logger = logging.getLogger(__name__)


@request(id=RequestId.OBJECTS_OF_TYPE)
class ObjectsOfTypeRequest(GuashClientRequest):
    """
    Request a list of files from inside a folder on the splash server
    """
    _fields = ['type']

    def __init__(self, obj_type: Optional[str] = None):
        super().__init__()
        self.type = obj_type

    def handle(self, session: 'GuashRemoteSession', respond: ResponseCallback) -> None:
        if not self.type:
            return
        respond(ObjectsOfTypeResponse(session.server.controller.get_objects_of_type(self.type)))


@response(id=ResponseId.OBJECTS_OF_TYPE)
class ObjectsOfTypeResponse(GuashServerResponse[ObjectsOfTypeRequest]):
    """
    Answer a client request for a list of files in a given folder
    """
    _fields = ['objects']

    def __init__(self, objects: Optional[List[str]] = None) -> None:
        super().__init__()
        self.objects = objects

    def handle(self, request: ObjectsOfTypeRequest, session: 'GuashLocalSession') -> None:
        assert isinstance(request, ObjectsOfTypeRequest)
        session.client.app.guash_root.image_objects = self.objects
