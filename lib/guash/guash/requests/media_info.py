import logging
from typing import Dict, Optional, TYPE_CHECKING

from guash.request import RequestId, ResponseId, GuashClientRequest, GuashServerResponse
from satnet.request import request, response, ResponseCallback

if TYPE_CHECKING:
    from guash.client.session import GuashLocalSession
    from guash.server.session import GuashRemoteSession

logger = logging.getLogger(__name__)


@request(id=RequestId.MEDIA_INFO)
class MediaInfoRequest(GuashClientRequest):
    """
    Request media information for a given filename
    """
    _fields = ['filename']

    def __init__(self, filename: Optional[str] = None) -> None:
        super().__init__()
        self.filename = filename

    def handle(self, session: 'GuashRemoteSession', respond: ResponseCallback) -> None:
        if not self.filename:
            return

        directory_attribute = 'mediaDirectory'
        folder = session.server.controller.get_object_attribute(session.server.controller.get_interpreter_name(), directory_attribute)
        if not folder or len(folder) != 1 or not isinstance(folder[0], str):
            logger.error('Invalid value for attribute {} of object python'.format(directory_attribute))
            return
        respond(MediaInfoResponse(session.server.controller.get_media_info(folder=folder[0], filename=self.filename), self.filename))


@response(id=ResponseId.MEDIA_INFO)
class MediaInfoResponse(GuashServerResponse[MediaInfoRequest]):
    """
    Answer a client request for a list of files in a given folder
    """
    _fields = ['media_info', 'filename']

    def __init__(self, media_info: Optional[Dict] = None, filename: Optional[str] = None) -> None:
        super().__init__()
        self.media_info = media_info
        self.filename = filename

    def handle(self, request: MediaInfoRequest, session: 'GuashLocalSession') -> None:
        assert isinstance(request, MediaInfoRequest)
        session.client.app.guash_root.update_media_info(self.media_info, self.filename)
        logger.debug('Received response to media info request: {}'.format(str(self.media_info)))
