import logging
import os
import threading
from enum import Enum
from time import sleep
from typing import Any, Dict, List, Tuple, Optional

import splash

logger = logging.getLogger(__name__)


class MediaType(Enum):
    UNKNOWN = 0x0
    IMAGE = 0x1
    VIDEO = 0x2
    V4L2 = 0x3


SPLASH_WAIT_DURATION = 0.01  # seconds


class SplashController:
    image_formats = ['.jpg', '.jpeg', '.png', '.bmp']
    video_formats = ['.mov', '.mp4', '.avi']

    def __init__(self):
        self._splash = splash
        self._sinks = {}  # type: Dict[str, Any]
        self._media_info_lock = threading.Lock()

    @staticmethod
    def wait_for_splash(test, tries: int = 10) -> bool:
        for i in range(tries):
            if test():
                return True
            sleep(SPLASH_WAIT_DURATION)
        return False

    def get_interpreter_name(self) -> str:
        """
        Get the name of the Splash embedded Python interpreter
        :return: The interpreter's name
        """
        return self._splash.get_interpreter_name()

    def set_object_attribute(self, object_name: str, attribute: str, value: Any) -> None:
        """
        Set an attribute of a splash object
        :param object_name: Name of the splash object
        :param attribute: Name of the attribute
        :param value: Value to set to the attribute
        :return:
        """
        try:
            self._splash.set_object(object_name, attribute, value)
        except Exception as e:
            logger.warning('Could not set attribute {} to splash object {}: {}'.format(attribute, object_name, e))

    def get_object_attribute(self, object_name: str, attribute: str) -> Any:
        """
        Set an attribute of a splash object
        :param object_name: Name of the splash object
        :param attribute: Name of the attribute
        :return: The attribute's value
        """
        try:
            value = self._splash.get_object_attribute(object_name, attribute)
        except Exception as e:
            logger.warning('Could not get attribute {} from splash object {}: {}'.format(attribute, object_name, e))
            return None
        else:
            return value

    def set_world_attribute(self, attribute: str, value: List[Any]) -> None:
        """
        Set a global property
        :param attribute: Name of the world attribute
        :param value: Value to set to the attribute (it's a list)
        """
        try:
            self._splash.set_world_attribute(attribute, value)
        except Exception as e:
            logger.warning('Could not set global property {}: {}'.format(attribute, e))

    def get_fps(self) -> float:
        timings = self._splash.get_timings()
        loop_scene_time = timings.get('loop_scene')
        if not loop_scene_time:
            return 0
        return 1e6 / loop_scene_time

    def get_master_clock(self) -> int:
        clock = self._splash.get_master_clock()
        if not clock or clock[1]:
            return 0
        return clock[2]

    def get_media_list(self, folder: str) -> List[Dict]:
        """
        Get list of files in a folder, optionally filter by extension. Also includes live inputs, like V4L2 devices
        :param folder: Folder to inspect
        :return: List of file media description inside a dictionary
        """
        extensions = SplashController.image_formats + SplashController.video_formats
        file_list = []  # type: List[Dict]
        folder = os.path.normpath(folder)
        try:
            folder_length = len(folder)
            for root, dirpath, files in os.walk(folder, followlinks=True):
                for file in files:
                    _, extension = os.path.splitext(file)
                    if not extensions or extension in extensions:
                        file_list.append({'text': os.path.join(root, file)[folder_length + 1:]})

            dev_folder = "/dev/"
            for root, dirpath, files in os.walk(dev_folder, followlinks=False):
                for file in files:
                    if file.startswith('video'):
                        filename = os.path.join(root, file)
                        file_list.append({'text': filename, 'live': True})

        except Exception as e:
            logger.warning(
                'Could not get files in folder {} with extension "{}": {}'.format(folder, str(extensions), e))
        finally:
            return file_list

    @staticmethod
    def get_media_type(filename: str) -> MediaType:
        _, extension = os.path.splitext(filename)
        if extension in SplashController.image_formats:
            return MediaType.IMAGE
        elif extension in SplashController.video_formats:
            return MediaType.VIDEO
        elif filename.startswith('/dev/video'):
            return MediaType.V4L2
        else:
            return MediaType.UNKNOWN

    def get_media_info(self, folder: str, filename: str) -> Dict:
        """
        Get media information for a given file, by creating a temporary Image object
        and getting its mediaInfo attribute
        :param folder: Media folder
        :param filename: Media filename
        :return: Dict of the media information
        """
        media_type = self.get_media_type(filename)
        media_info = {}
        info_media_name = 'info_media'

        if media_type == MediaType.IMAGE:
            media_splash_type = 'image'
        elif media_type == MediaType.VIDEO:
            media_splash_type = 'image_ffmpeg'
        elif media_type == MediaType.V4L2:
            media_splash_type = 'image_v4l2'
        else:
            logger.warning('Unknown media type')
            return media_info

        # Wait for a previous call to this method to be finished
        self._media_info_lock.acquire()

        self._splash.set_world_attribute('addObject', [media_splash_type, info_media_name])

        # Check whether the object has been created
        def test_object_in_list():
            if info_media_name in self._splash.get_object_list():
                return True
            return False
        if not self.wait_for_splash(test_object_in_list, 10):
            return media_info

        fullpath = os.path.join(folder, filename)
        if not os.path.exists(fullpath):
            logger.warning('Trying to get info from media {} but the path does not exist.'.format(filename))
            self._splash.set_world_attribute('deleteObject', [info_media_name])
            self._media_info_lock.release()
            return media_info

        # Set the media parameters, and go forward once the set is effective
        if media_type in [MediaType.IMAGE, MediaType.VIDEO]:
            self._splash.set_object_attribute(info_media_name, 'file', [fullpath])
            if not self.wait_for_splash(lambda: self._splash.get_object_attribute(info_media_name, 'file')[0] == fullpath):
                self._splash.set_world_attribute('deleteObject', [info_media_name])
                self._media_info_lock.release()
                return media_info
        elif media_type == MediaType.V4L2:
            self._splash.set_object_attribute(info_media_name, 'device', [filename])
            self._splash.set_object_attribute(info_media_name, 'doCapture', [1])
            if not self.wait_for_splash(lambda: self._splash.get_object_attribute(info_media_name, 'device')[0] == filename):
                self._splash.set_world_attribute('deleteObject', [info_media_name])
                self._media_info_lock.release()
                return media_info

        if not self.wait_for_splash(lambda: len(self._splash.get_object_attribute(info_media_name, 'mediaInfo', True)) != 0, 100):
            self._splash.set_world_attribute('deleteObject', [info_media_name])
            self._media_info_lock.release()
            return media_info

        media_info = self._splash.get_object_attribute(info_media_name, 'mediaInfo', True)
        self._splash.set_world_attribute('deleteObject', [info_media_name])
        self._media_info_lock.release()

        # Needed to avoid collision with potential kivy property names...
        return {'media_{}'.format(k): v for (k, v) in media_info.items()}

    def load_project(self, project_file: str) -> None:
        if project_file:
            logger.info('Loading project file {}'.format(project_file))
            self._splash.set_world_attribute('loadProject', project_file)

    def save_project(self, project_file: str) -> None:
        if project_file:
            logger.info('Saving current project to: {}'.format(project_file))
            self._splash.set_world_attribute('saveProject', project_file)

    def load_media(self, media_file: str) -> None:
        pass

    @staticmethod
    def check_object_exist(objectname: str) -> bool:
        object_list = splash._splash.get_object_list()
        if objectname in object_list:
            return True
        else:
            return False

    def preview_media(self, filename: str) -> None:
        active_media_dir = self._splash.get_object_attribute(self.get_interpreter_name(), 'activeMediaDirectory')
        if not active_media_dir or len(active_media_dir) != 1 or not isinstance(active_media_dir[0], str):
            return

        media_type = self.get_media_type(filename)
        preview_media_name = 'preview_media'

        if media_type == MediaType.IMAGE:
            media_splash_type = 'image'
        elif media_type == MediaType.VIDEO:
            media_splash_type = 'image_ffmpeg'
        elif media_type == MediaType.V4L2:
            media_splash_type = 'image_v4l2'
        else:
            logger.warning('Unknown media type')
            return

        object_list = self._splash.get_object_list()
        preview_media_type = self._splash.get_object_types().get(preview_media_name)

        if preview_media_type != media_splash_type and preview_media_name in object_list:
            preview_media_existed = True
            self._splash.set_world_attribute('replaceObject', [preview_media_name, media_splash_type])
            # Check whether the object has been replaced
            while self._splash.get_object_types().get(preview_media_name) != media_splash_type:
                sleep(SPLASH_WAIT_DURATION)
        else:
            preview_media_existed = False
            self._splash.set_world_attribute('addObject', [media_splash_type, preview_media_name])

        # Check whether the object has been created
        while True:
            object_list = self._splash.get_object_list()
            if preview_media_name in object_list:
                break
            sleep(SPLASH_WAIT_DURATION)

        fullpath = os.path.join(active_media_dir[0], filename)
        if not os.path.exists(fullpath):
            logger.warning('Trying to preview media {} but it is not activated.'.format(filename))
            return

        if media_type in [MediaType.IMAGE, MediaType.VIDEO]:
            self._splash.set_object_attribute(preview_media_name, 'file', [fullpath])
        elif media_type == MediaType.V4L2:
            self._splash.set_object_attribute(preview_media_name, 'device', [filename])
            self._splash.set_object_attribute(preview_media_name, 'doCapture', [1])

        if not preview_media_existed and not self.open_sink('preview_media'):
            logger.warning('Could not create or find preview sink')
            return

    def open_sink(self, sink_name: str) -> Any:
        sink = self._sinks.get(sink_name)
        if sink:
            sink.link_to(sink_name)
            sink.open()
            return sink
        sink = self._splash.Sink()
        if not sink:
            return None
        sink.link_to(sink_name)
        sink.set_size(256)
        sink.keep_ratio(True)
        sink.set_framerate(5)
        sink.open()
        self._sinks[sink_name] = sink

        return sink

    def close_sink(self, sink_name: str) -> None:
        try:
            sink = self._sinks.get(sink_name)  # self._sinks.pop(sink_name)
        except KeyError:
            logger.debug('Splash sink {} is not open.'.format(sink_name))
        else:
            if sink:
                sink.close()
                sink.unlink()

    def get_sink_render(self, sink_name: str) -> Tuple[bytes, int, int]:
        sink = self._sinks.get(sink_name)
        if sink:
            buffer = sink.grab()
            size = sink.get_size()
            if buffer and size is not None:
                return bytes(buffer), size[0], size[1]
        return b'', 0, 0

    def get_objects_of_type(self, obj_type: str) -> List[str]:
        return self._splash.get_objects_of_type(obj_type)
