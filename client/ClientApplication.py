import logging

from guash.client.app.app import GuashApp
from guash.client.client import GuashClient
from satnet.adapters.websockets.client import WSClient
from satnet.message import MessageParser

logger = logging.getLogger(__name__)


class ClientApplication:
    """
    Main client application class
    """

    def __init__(self) -> None:
        from guash.all_serializable import import_all
        import_all()
        self._client = GuashClient(adapter=WSClient(message_parser=MessageParser()))
        self._gui = GuashApp(self._client)
        self._client.app = self._gui

    def run(self) -> None:
        try:
            self._gui.run()
        except KeyboardInterrupt as e:
            logger.error(e)
